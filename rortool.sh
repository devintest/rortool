#!/bin/bash
# Dev tools for rails configuration menu.
# Designed for container usage, should work on the host.
AboutThis="
Create software modules which behave as one of the following:
1 INPUT:    User to input data such as application name to create or load, from an uploaded file or an external stream. 
2 VALIDATE: Ensures entered data is valid.  
4 PROCESS:  Perform action required to fulfill required process.
3 OUTPUT:   Display output to user, create file for download or provide an output stream.
"
# ---------------------- Arrays & Variables ---------------------
# Curser colour options
RED='\033[0;41;30m'
STD='\033[0;0;39m'


# Ensures application starts in the users home folder.
    cd $HOME

# create project folder
    mkdir -v -p $HOME/projects

# 
declare -a DataTypes=("binary" "boolean" "date" "datetime" "decimal" "float" "integer" "string" "text" "time" "timestamp")

DMTupdate(){
# Change working directory to current repository location
  cd ~/rortool/
# Check repository for diiferences and retrieve any found
  git pull 
# Change working directory to current project  
  cd $HOME/projects/$ProjectName
}

# ---------------- Install LXD ?---------------

# ---------------- Setup Rails ----------------

SetupRbenv(){
# Install desired verion of Ruby and Rails 
# Install Ruby Version Manager which allows any version of Ruby and Rails 
    sudo apt install rbenv
    
# Install Ruby Version XXX

# Install Rails version XXX
}
# ---------------------- Project Creation ---------------------
# Create a new rails project & git init

# --- INPUTS ---

NameProject(){
# Capture the desired project name
    read -rp "Name for this project: " ProjectName
    FieldNotEmpty
}

ProjectSelect(){
# select a project to load
    $DisplayProjects
# Capture user input  
    read -rp "Choose a project " ProjectName
    FieldNotEmpty
    cd ~/projects/$ProjectName
#    Change2directory
}

ChooseContainer(){  
# Capture user input  
  read -rp "Choose a target container " ContainerName
# Validate data was entered
    if [ "$ContainerName" == "" ] 
    then    
# Request user enter a name and stop 
    TellUser="Enter a name please"
    MainMenu
# Otherwise display what was entered    
    else
    echo "You entered:" $ContainerName
    fi
} 

# --- VALIDATE ---

FieldNotEmpty(){
# alled by other applications to validate data was entered.
    if [ "$ProjectName" == "" ]; 
    then    
# Request user enter a name and RETURN TO the main menu 
    TellUser="Enter a name please"
    MainMenu
# Otherwise display what was entered    
    else
    TellUser="You entered: $ProjectName"
    fi
}

ValidateProjectCreation(){
#  confirm the existance of new project name and then initialize git.
if [ -f "~/projects/$ProjectName" ]; 
then 
# Initialize Git within the created folder
#    git init $HOME/projects/$ProjectName
# Change to new directory    
	cd ~/projects/$ProjectName 
# Inform user    
	lastmessage="$ProjectName is ready.";
else 
	lastmessage="That did not work, check above for clues."
fi
}


# --- PROCESS --- 

# Display existing projects
DisplayProjects="ls $HOME/projects/"

# Change to desired project folder
Change2directory="cd $HOME/projects/$ProjectName"

CreateProject(){
# Create the project directory
    mkdir -v -p $HOME/projects/$ProjectName/
# Change working directory to current project  
    cd ~/projects/$ProjectName/
# initialise new rails app in folder	
	rails new . 
}

RailsUp(){
# Start the rails server and configure web interface for localhost port 3000.  
    rails s -b 0.0.0.0 #-d to run in the bakground
}

# ---------------------- Creation --------------------

# 

SetupHomePage(){
# Creates an index page which will contain links to the various resource pages.
    rails g controller $ProjectName index
    
# Correct routing for new page
    sed -i "3s/.*/root '$ProjectName#index'/" ~/projects/$ProjectName/config/routes.rb
}


NewTable(){
#create scaffolding for Database object CRUD:

  read -rp "Name for this new DB table: " Table_Name
  read -rp "Name for this new field: " Field_Name 
  printf '%s\n' "${DataTypes[@]}"
#  printf '%s' "${DataTypes[@]}"
  read -rp "Please enter the required Data Type from the above list: " Data_Type
  
  rails g scaffold $Table_Name $Field_Name:$Data_Type
  rake db:migrate

  #MoreTables 
  #AddField
}  

MoreTables(){
# Add more tables
  read -rp "Add another field to this table? Y/N" More_Fields
  
}  

STARproject(){
# Demonstration 
  ProjectSelect
  rails g scaffold S_T_A_R Scenario_Name:text Author:text Creation_Date:date Industry:text Requirement:text Outline:text Scenario:text Task:text Action:text Result:text Lessons_Learned:text Comments:text
  rake db:migrate
}  

newcontroller(){
    read -rp "Name for this new DB table: " Table_Name
    rails generate controller $Table_Name new
}

newmodel(){
# Create a new model for the table and define expected data type
# Capture new field name
    read -rp "Name for this new field: " Field_Name 
# Display available data types
    printf '%s\n' "${DataTypes[@]}"
# Capture data type from user
    read -rp "Data Type: " Data_Type
# Have Rails fulfil request with provided data
    rails generate model $Table_Name $Field_Name:$Data_Type
# Update database changes
    rake db:migrate
}


# Add a new column:datatype to an existing field: 
NewColumn(){
    read -rp "Add a field to which table?" ThisTable
    read -rp "name this new field: " ThisItem
# Display available data types
    printf '%s\n' "${DataTypes[@]}"
    read -rp "Data Type: " Data_Type
    rails generate migration Add$ThisItem To $ThisTable
    $ThisItem:$Data_Type
    rake db:migrate
}
# ------------------- Removal ---------------------
#Delete a scaffold and database entry:fibre_delivery_activity
DelTable(){
  ProjectSelect
  read -rp "Name of DB table to Delete/Drop: " Table_Name
  rails destroy $Table_Name
  rake db:migrate
  rails db:drop:$Table_Name
  rake db:migrate
}

DelScaffold(){
  read -rp "Name of the Scaffold to remove: " Scaff_Name
  rails destroy $Scaff_Name
  rake db:migrate
}

DeleteProject(){
    ProjectSelect
    rm -R ~/projects/$ProjectName
}


# ----------------- experimental features --------------------

Port3kInUse(){
# list servers using port 3000
    lsof -wni tcp:3000
}

KillThePID(){
# Kill a running Process ID
# enter PID to kill    
    read -rp "Enter the PID you want to kill " PidKill
# copy the PID
    kill -9 $PidKill

# Another way of doing it: You can also use the following command to kill all running apps with rails in the name: killall -9 rails

}

wasTryATing(){
# Scripted execution 
    NewRorApp
    GitInit
    #ProjectSelect
    SetupHomePage
    RailsUp
}

TryATing(){
# Demonstration 
    ProjectSelect
    rails g scaffold TestLab Scenario_Name:text Author:text Creation_Date:date Requirement:text Outline:text Scenarios:text Tasks:text Comments:text RuleName:text RuleDescription:text CustomerId:text ChannelName:text ExpectedResponse:text CustomerPassRequirements:text 
    rake db:migrate
    SetupHomePage
    RailsUp    
}  

# 3 -------------------- GIT Commands -------------------------
# Set git push default to stop that message appering
  git config --global push.default simple
clear


file2git(){
	read -rp "Enter the name of the file or,' . 'for all files: " gitfile 
}
all2git(){
	gitfile="."
}
gitpull(){
	git pull
}
gitadd(){
	git add $gitfile
}
gitcommit(){
	git commit 
}
gitpush(){
	git push 
}

gitUserConfig(){
	read -rp "Enter your email: " gitmail
	git config user.email "$gitmail"
	read -rp "Enter your username: " gituser
	git config user.name "$gituser"
}
newgit(){
	read -rp "Enter the name of your new repository " GitRepo
	read -rp "Enter the path for your new repository or leave empty to place it in your Documents " NewDir
	if [ -z "$NewDir" ]; then NewDir=/home/$USER/Documents;
	else 
	echo " Creating in $NewDir "
	fi
	PathFile=$NewDir/$GitRepo
	echo $PathFile
	git init $PathFile
}


#----------------------------------------------------------
gitmenu(){
git_menu() {

	echo ""	
	echo " Ruby on Rails Dev Tool GIT "
	echo " Common GIT commands in a simple menu. "
    echo "------------------------"
	echo "1.  Update the repository within the current project  folder "
	echo "2.  track a single file for changes. "
	echo "3.  Commit your changes "
	echo "4.  Push commited changes to a remote repository "
	echo "5.  Add all file changes & Commit "
	echo "6.  Add all files, Commit & Push "
	echo "7.  User Configuration.                       Identify yourself to Git "
	echo "8.  Create and intialise a new repository in a location of your choice "
	echo "9.  Add all files changes & Stash (NDY) "
	echo "99. Return to main menu " 
    echo "------------------------"

}

git_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) gitpull ;;
		2) file2git && gitadd ;;
		3) gitcommit ;;
		4) gitpush ;;
		5) all2git && gitadd && gitcommit ;;
		6) all2git && gitadd && gitcommit && gitpush ;;
		7) clear && gitUserConfig ;;
		8) newgit ;;
		99) clear && MainMenu ;;
		*) echo -e "${RED} Sorry, can't do $choice. ${STD}" && sleep 2
	esac
}

while true
do
	git_menu
	git_options
done
}


# ----------------- Display menu options -----------------
MainMenu(){
# Create ROR apps.
    MenuTitle="Host-Side Ruby on Rails Dev Tool" 
    Description="Manage & develop RoR Projects within a container from a BASH menu driven interface"
    
    
show_menus() {

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo " $ProjectName "
	echo "-----------------------------------"
	echo "0.  Update RoR Development Tool "
	echo "10. New                  Create a new RoR project "
    echo "11. Load an existing project."
    echo "12. Create App"
    echo "13. List existing projects"
	echo "20. Add a home page "
	echo "30. Start Server         Start the Rails server  "
    echo "40. Stop a Server by Killing it's PID!  "
	echo "4.  Add a new page "
	echo "5.  Add a data field to an existing page "
	echo "6.  Git Menu             Add, Commit, Push & other GIT options "
	echo "70. Delete a page " 
	echo "72. Delete Scaffold "
	echo "75. Delete Project"
	echo "8.  Make a S.T.A.R capture form "
    echo "90. Trying a Thing"
	echo "99. Quit "
	echo " "
	echo " $TellUser " 
	echo ""
 
}

read_options(){
	local choice
	read -p "Enter the desired item number: " choice
	case $choice in

		0)  clear && DMTupdate ;;
		10) NameProject && CreateProject && ValidateProjectCreation ;;
        11) ProjectSelect ;;
        12) NewRorApp ;;
        13) $DisplayProjects ;;
		20) SetupHomePage ;;
		30) ProjectSelect && ChooseContainer && RailsUp ;;
        40) Port3kInUse && KillThePID ;;
		4)  NewTable ;;
		5)  NewColumn ;;
		6)  clear && gitmenu ;;
		70) DelTable ;;
		72) DelScaffold ;;
		75) DeleteProject ;;
		8)  STARproject ;;
		90) TryATing ;;
        99) clear && echo " Goodbye $USER! " && exit 0;;
		*) echo -e "${RED} What is $choice supposed to mean? ${STD}" && sleep 2
	esac
}


# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

# This is the selector, used to run through scenarios

MainMenu

#ProjectList
#NameProject
#NewRorApp
#ValidateProjectCreation
#ProjectSelect
#GitInit
#TryATing
#SetupHomePage
#RailsUp




